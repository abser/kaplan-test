<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'kaplan');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'tY8-ssk.(fFqC+nGd#3q6L4qd4^240r(?G_N0u0N4.:N3Y7mwz9A-b{u XV{8W_u');
define('SECURE_AUTH_KEY',  '|OPa>IimC&;)1Oy~Fjg:myLR)dVn[-w?L! -,?CGMeblu1l6Tg+(p-8FLCOoYD|z');
define('LOGGED_IN_KEY',    'Lnqs 9&g}rS;fC&v*%04z{rd8Awr]Y_|D-8AaX+ZLM6d#yH@E+fi;B|.qcRf`&tm');
define('NONCE_KEY',        'FIsxFfLG4|E3]J#~IMCfTSN ou8IX4Q^us?^9# cjkU}4zO<aq@$1hB+W+I>*ps~');
define('AUTH_SALT',        'f.V uJwZy&-b<&@?(Q{URapL+]d7y)|l<@+Ev-OWUk,u1%F:-`q=zQFc61SvPq>=');
define('SECURE_AUTH_SALT', '&J+asl|,{{Q/]pl1l#qX1E1V>:/e/RL*0-CB.QURk=bs,5@nRZ{m| 7bfvOk0wo,');
define('LOGGED_IN_SALT',   '- ZK%xAj)6oKYcPn-xjo-~Q@`[sKOaC>}>`q6}K$^eTy-~bna|B>N4OA-l:Lo^j^');
define('NONCE_SALT',       'OK*N|/@6@vyY~T-lpH^B_{-t&VKmvL]t!i6J`H=`HH36t!5?f.g&5{+`pTtm2UG)');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
