<?php
/**
  Template Name: newsfeed 
  Description: Show news feed from BBC
 */

 ?>
 <?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
  /*
*  How to find a feed based on a query.
*/

google.load("feeds", "1");

function OnLoad() {
  // Query for president feeds on cnn.com
  var query = 'site:cnn.com president';
  google.feeds.findFeeds(query, findDone);
}

function findDone(result) {
  // Make sure we didn't get an error.
  if (!result.error) {
    // Get content div
    var content = document.getElementById('feed');
    var html = '';

    // Loop through the results and print out the title of the feed and link to
    // the url.
    for (var i = 0; i < result.entries.length; i++) {
      var entry = result.entries[i];
      html += '<p><a href="/feed/v1/' + entry.url + '">' + entry.title + '</a></p>';
    }
    content.innerHTML = html;
  }
}

google.setOnLoadCallback(OnLoad);


</script>
<!-- start content container -->
<div class="row dmbs-content">

    <?php //left sidebar ?>
    <?php get_sidebar( 'left' ); ?>
    

    <div class="col-md-<?php devdmbootstrap3_main_content_width(); ?> dmbs-main">
	
		<div class="col-md-4">
			<h2>News Feed 1</h2>
                        <div id="feed"></div>
		</div>
		 
		 <div class="col-md-4">
			<h2> Business</h2>
			 <ul>
				<li> <a  href="#"> News List </a> </li>
				<li> <a  href="#"> News List </a> </li>
				<li> <a  href="#"> News List </a> </li>
				<li> <a  href="#"> News List </a> </li>
			</ul>
		</div>
		
		 	<div class="col-md-4">
			<h2>Technology</h2>
			 <ul>
				<li> <a  href="#"> News List </a> </li>
				<li> <a  href="#"> News List </a> </li>
				<li> <a  href="#"> News List </a> </li>
				<li> <a  href="#"> News List </a> </li>
			</ul>
		</div>
		 

    </div>

    <?php //get the right sidebar ?>
    <?php get_sidebar( 'right' ); ?>

</div>
<!-- end content container -->

<?php get_footer(); ?>

